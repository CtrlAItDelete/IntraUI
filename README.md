# 创建示范 :

	new_ui = new_ui or IntraUI:new({
		name = "my_test_ui",
		keybind = "f1",
		w = 300,
		h = 500,
		main_menu = "main_ui",
		Main = {
			name = "main_ui",
			title = "Player",
			color = Color.white,
			toggle_menu = {
				name = "toggle_menu",
				type = "toggle",
				text = "Toggle Menu",
				color = Color.white,
				state = false,
				callback = this_is_callback_func,
				x = 12,
				y = 50,
				size = 25,
				bg_w = 185
			},
			
			switch_next_menu = {
				name = "switch_next_menu",
				type = "button",
				text = "Switch Next Menu",
				color = Color.white,
				callback = SwitchMenu,
				id = "money",
				x = 12,
				y = 80,
				size = 25
			},

			slider_t = {
				name = "slider_t",
				type = "slider",
				text = "test1",
				color = Color.white,
				max = 100,
				min = 0,
				callback = test,
				state = 100,
				w = 200,
				x = 12,
				y = 110
				menu = {
					name = "next_menu",
					title = "Menu 2",
					next_menu_buton = {
						name = "next_menu_buton",
						type = "button",
						text = "test",
						color = Color.white,
						x = 12,
						y = 80,
						size = 25,
					}
				}
			}
		},
		Money = {
			name = "money",
			title = "Money",
			switch_next_menu = {
				name = "test",
				type = "button",
				text = "test",
				color = Color.white,
				callback = SwitchMenu,
				id = "main_ui",
				x = 12,
				y = 200,
				size = 25
			}			
		}
	})

内部函数调用示范 :
toggle_state = new_ui:get_state("toggle_menu")

# 基本类型

## toggle 类型
*在ui上显示一个滑动框，点击可切换激活状态，然后执行callback所填写的函数以及传入一个boolean实参*
- name, ui的id  string(必须填写)
- type, 类型  "toggle" string(必须填写)
- text, 框内的文字  string(必须填写)
- color, 文字颜色  userdata(示范 : Color.green)
- state, 默认为打开还是关系，不填写就默认为关闭  boolean
- callback, 触发点击时执行的函数  function value
- x, 此ui所处于的x轴位置  number(必须填写)
- y, 此ui所处于的y轴位置  number(必须填写)
- bg_w, 鼠标触发区域, 当鼠标移动到此ui上显示的透明色白框长度  number

## button 类型
*在ui上显示一个按钮，点击即可触发callback填写的函数*
- name, ui的id  string(必须填写)
- type, 类型  "button" string(必须填写)
- text, 框内的文字  string(必须填写)
- color, 文字颜色  userdata(示范 : Color.green)
- callback, 触发点击时执行的函数  function value
- x, 此ui所处于的x轴位置  number(必须填写)
- y, 此ui所处于的y轴位置  number(必须填写)

## slider 类型
*在ui上显示一个数值条，拖动可改变数值然后触发callback填写的函数并传入一个number实参*
- name, ui的id  string(必须填写)
- type, 类型  "slider" string(必须填写)
- text, 框内的文字  string(必须填写)
- color, 文字颜色  userdata(示范 : Color.green)
- max, 最大数值
- min, 最小数值
- w, slider板块的长度，不填写默认为200
- state, 默认数值，不填写就默认为0  number
- callback, slider数值被改变时执行的函数  function value
- x, 此ui所处于的x轴位置  number(必须填写)
- y, 此ui所处于的y轴位置  number(必须填写)

# 内部函数

## show
显示ui，使用非内部按键触发可使用

## hide
关闭ui，默认按"esc"在内部执行，可做外部调用

## get_state
获取当前ui的状态
- name 需要获取状态ui的name (string)

## set_state
更改ui的当前状态
- name 需要更改ui的name (string)
- state 需要更改的状态

## get_type
获取ui类型
- name 需要获取ui类型的name (string)

## switch_menu
切换菜单
- menu_name 需要切换的菜单name (string)
